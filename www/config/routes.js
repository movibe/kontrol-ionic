app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider

        // Dashboard
        .state('main', {
            url: "/",
            controller: "DashboardController",
            templateUrl: "bundles/Main/templates/dashboard.html"
        })

        // Spotify
        .state('spotify', {
            url: "/spotify",
            abstract: true,
            templateUrl: "bundles/Spotify/templates/_tabs.html"
        })
            // Tracks Tab
            .state('spotify.playlists', {
                url: '/playlists',
                views: {
                    'spotify-tracks': { // <-- Which tab to be in. Matches with 'ion-nav-view' 'name' attribute
                        controller: 'SpotifyPlaylistsController',
                        templateUrl: 'bundles/Spotify/templates/playlists.html'
                    }
                }
            })
            .state('spotify.playlist', {
                url: '/playlists/:uri',
                views: {
                    'spotify-tracks': {
                        controller: 'SpotifyPlaylistController',
                        templateUrl: 'bundles/Spotify/templates/playlist.html'
                    }
                }
            })
            .state('spotify.playTrack', {
                url: '/play/:uri',
                views: {
                    'spotify-tracks': {
                        controller: 'SpotifyPlayingController',
                        templateUrl: 'bundles/Spotify/templates/playing.html'
                    }
                }
            })
            .state('spotify.playTrackAtPosition', {
                url: '/play/:playlistUri/:trackUri',
                views: {
                    'spotify-tracks': {
                        controller: 'SpotifyPlayingController',
                        templateUrl: 'bundles/Spotify/templates/playing.html'
                    }
                }
            })

            // Tracklist tab
            .state('spotify.tracklist', {
                url: '/tracklist',
                views: {
                    'spotify-tracklist': {
                        controller: 'SpotifyTracklistController',
                        templateUrl: 'bundles/Spotify/templates/tracklist.html'
                    }
                }
            })

        //// setup an abstract state for the tabs directive
        //.state('tab', {
        //    url: "/tab",
        //    abstract: true,
        //    templateUrl: "templates/tabs.html"
        //})
        //
        //// Each tab has its own nav history stack:
        //
        //.state('tab.dash', {
        //    url: '/dash',
        //    views: {
        //        'tab-dash': {
        //            templateUrl: 'templates/tab-dash.html',
        //            controller: 'DashCtrl'
        //        }
        //    }
        //})
        //
        //.state('tab.chats', {
        //    url: '/chats',
        //    views: {
        //        'tab-chats': {
        //            templateUrl: 'templates/tab-chats.html',
        //            controller: 'ChatsCtrl'
        //        }
        //    }
        //})
        //.state('tab.chat-detail', {
        //    url: '/chats/:chatId',
        //    views: {
        //        'tab-chats': {
        //            templateUrl: 'templates/chat-detail.html',
        //            controller: 'ChatDetailCtrl'
        //        }
        //    }
        //})
        //
        //.state('tab.account', {
        //    url: '/account',
        //    views: {
        //        'tab-account': {
        //            templateUrl: 'templates/tab-account.html',
        //            controller: 'AccountCtrl'
        //        }
        //    }
        //});

});