app.controller('SpotifyTracklistController', function($rootScope, $scope, $stateParams, $state, $http, $interval, CONFIG, SPOTIFY, IO, SpotifyService, Tracklist)
{
    function init()
    {
        $scope.tracklist = Tracklist.query(function(tracks) {
            console.log(tracks);
        });

        initEvents();
    }


    /* ================================================= *
     * Public Functions
     * ================================================= */




    /* ================================================= *
     * Private Functions
     * ================================================= */

    function initEvents()
    {
        IO.on(SPOTIFY.events.tracklistChanged, function(tracks) {
            console.log("Tracklist changed:", tracks);
        });
    }


    /* ================================================= *
     * Init
     * ================================================= */
    init();

});