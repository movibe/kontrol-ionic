app.controller('SpotifyPlaylistsController', function($rootScope, $scope, Playlist)
{
    function init()
    {
        $scope.playlists = Playlist.query(function(playlists) {
            $rootScope.playlists = playlists;
        });

        $scope.loadingUntilResolved($scope.playlists);
    }


    /* ================================================= *
     * Public Functions
     * ================================================= */




    /* ================================================= *
     * Private Functions
     * ================================================= */




    /* ================================================= *
     * Init
     * ================================================= */
    init();

});