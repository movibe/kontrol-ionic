app.service('SpotifyService', function($http, CONFIG)
{
    this.playTrack = function(uri, uri2)
    {
        var urlEnd = (uri && uri2) ? uri + '/' + uri2 : uri;
        return $http.post(CONFIG.api + 'mopidy/playback/play/' + urlEnd);
    }

    this.resume = function()
    {
        return $http.post(CONFIG.api + 'mopidy/playback/resume');
    }

    this.pause = function()
    {
        return $http.post(CONFIG.api + 'mopidy/playback/pause');
    }

    this.stop = function()
    {
        return $http.post(CONFIG.api + 'mopidy/playback/stop');
    }

    this.fetchCurrentTrack = function(successCallback, errorCallback)
    {
        successCallback = successCallback || function() {};
        errorCallback = errorCallback || function() {};

        return $http.get(CONFIG.api + 'mopidy/playback/current-track').success(function(track) {
            successCallback(track);
        }).error(function(error) {
            errorCallback(error);
        });
    }

    this.fetchCurrentTimePosition = function(successCallback, errorCallback)
    {
        successCallback = successCallback || function() {};
        errorCallback = errorCallback || function() {};

        return $http.get(CONFIG.api + 'mopidy/playback/time-position').success(function(track) {
            successCallback(track);
        }).error(function(error) {
            errorCallback(error);
        });
    }

    this.fetchAlbumArtwork = function(track, successCallback, errorCallback)
    {
        successCallback = successCallback || function() {};
        errorCallback = errorCallback || function() {};

        var url = "http://itunes.apple.com/search";
        var term = track.artists[0].name + " - " + track.album.name;
        var resolution = 600; // 1200

        $http.jsonp(url, {
            params: {
                callback: 'JSON_CALLBACK',
                media: "music",
                entity: "album",
                country: "us",
                term: term
            }
        }).then(function(data) {
            var album = data.data.results[0];
            var albumArtwork = album.artworkUrl100.replace('.100x100-', '.' + resolution + 'x' + resolution + '-');

            successCallback(albumArtwork);
        }, function(error) {
            errorCallback(error);
        })
    }

    this.minutesString = function(milliseconds)
    {
        var minutes = Math.floor((milliseconds / 1000) / 60);
        var seconds = (milliseconds / 1000) % 60;
        return minutes + ":" + seconds;
    }

    this.positionPercent = function(track)
    {
        return (track.timePosition / track.length) * 100;
    }

});